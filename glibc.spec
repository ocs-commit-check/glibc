# glibc build cost long time, add this condition to get fast build for dev
%bcond_with fastbuild

%if %{with fastbuild}
%bcond_with source
%bcond_with all_langpack
%bcond_with benchtests
%bcond_with werror
%bcond_with valgrind
%bcond_with testsuite
%else
%bcond_without source
%bcond_without all_langpack
%bcond_without benchtests
%bcond_without werror
%bcond_without valgrind
%bcond_without testsuite
%endif

%ifarch loongarch64
%undefine with_testsuite
%endif

%bcond_with bootstrap
%if %{with bootstrap}
%undefine with_benchtests
%undefine with_werror
%undefine with_valgrind
%endif

# Default: Always run valgrind tests if there is architecture support.
# The valgrind tests does not work on riscv
%ifnarch %{valgrind_arches}
%undefine with_valgrind
%endif

# if we enable annobin plugin and enable its work, building of glibc will error with 'error: -D_FORTIFY_SOURCE not defined [-Werror]'
# if we enable annobin plugin and disale its work(like centos or fedora), we think it is meaningless
# so we disable annobin supports in whole building cycle directly.
%undefine _annotated_build

%undefine _auto_set_build_flags
%global locale_rx eo syr *_*
%global __filter_GLIBC_PRIVATE 1
%global target %{_target_cpu}-%{_vendor}-linux
%bcond_without nscd

# debuginfo script relies debugedit does not change build id
%global _no_recompute_build_ids 1
%undefine _unique_build_ids

%global glibc_post_funcs %{expand:
function post_exec (msg, program, ...)
  local pid = posix.fork ()
  if pid == 0 then
    posix.exec (program, ...)
    io.stdout:write (msg)
    assert (nil)
  elseif pid > 0 then
    posix.wait (pid)
  end
end

function call_ldconfig ()
  post_exec("Error: call to %{_prefix}/sbin/ldconfig failed.\n",
            "%{_prefix}/sbin/ldconfig")
end

function update_gconv_modules_cache ()
  local iconv_dir = "%{_libdir}/gconv"
  local iconv_cache = iconv_dir .. "/gconv-modules.cache"
  local iconv_modules = iconv_dir .. "/gconv-modules"
  if posix.utime(iconv_modules) == 0 then
    if posix.utime (iconv_cache) == 0 then
      post_exec ("Error: call to %{_prefix}/sbin/iconvconfig failed.\n",
                 "%{_prefix}/sbin/iconvconfig",
                 "-o", iconv_cache,
                 "--nostdlib",
                 iconv_dir)
    else
      io.stdout:write ("Error: Missing " .. iconv_cache .. " file.\n")
    end
  end
end}

Summary: The GNU libc libraries
Name: glibc
Version: 2.38
Release: 28%{?dist}

License: LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL
URL: http://www.gnu.org/software/glibc/
Source0: https://ftp.gnu.org/gnu/glibc/%{name}-%{version}.tar.xz
Source1: bench.mk
Source2: glibc-bench-compare
# customize debuginfo 
Source4: wrap-find-debuginfo.sh
Source5: generate-langpack.lua
Source6: nscd.conf

# CVE-2023-4911
Patch0001: CVE-tunables-Terminate-if-end-of-input-is-reached-CVE-20.patch
Patch0002: CVE-2023-4806-getaddrinfo-Fix-use-after-free-in-getcanonname-CVE-2.patch
Patch0003: CVE-2023-5156-Fix-leak-in-getaddrinfo-introduced-by-the-fix-for-CV.patch
Patch0004: CVE-2023-4527-Stack-read-overflow-with-large-TCP-res.patch
Patch0005: CVE-2023-6246-syslog-Fix-heap-buffer-overflow-in-__vsyslog_interna.patch
Patch0006: CVE-2023-6779-syslog-Fix-heap-buffer-overflow-in-__vsyslog_interna.patch
Patch0007: CVE-2023-6780-syslog-Fix-integer-overflow-in-__vsyslog_internal-CV.patch
Patch0008: CVE-2024-2961-iconv-ISO-2022-CN-EXT-fix-out-of-bound-writes-when-w.patch
Patch0010: backport-libio-Fix-oversized-__io_vtables.patch
Patch0011: CVE-2024-33599-nscd-Stack-based-buffer-overflow-in-n.patch
Patch0012: CVE-2024-33600-nscd-Do-not-send-missing-not-found-re.patch
Patch0013: CVE-2024-33600-nscd-Avoid-null-pointer-crashes-after.patch
Patch0014: CVE-2024-33601-CVE-2024-33602-nscd-netgroup-Use-two-.patch
Patch0015: backport-x86-string-Fixup-alignment-of-main-loop-in-str-n-cmp.patch
Patch0016: https://github.com/sailfishos-mirror/glibc/commit/950891b5e7a5307272da3e632832ac9da4c9eeec.patch
Patch0017: https://github.com/sailfishos-mirror/glibc/commit/6fd215d6ae9a4a6e75f7ea18d89db6a10f158eaf.patch


Patch3000: glibc-python3.patch
Patch3001: glibc-disable-werror-tst-realloc.patch
Patch3002: glibc-rh827510.patch
Patch3003: 3003-add-GB18030-2022-charmap-support.patch
Patch3004: LoongArch-Redefine-macro-LEAF-ENTRY.patch
Patch3005: LoongArch-Add-minuimum-binutils-required-version.patch
Patch3006: Loongarch-Add-ifunc-support-and-add-different-versio.patch
Patch3007: elf-Add-new-LoongArch-reloc-types-101-to-108-into-el.patch
Patch3008: LoongArch-elf-Add-new-LoongArch-reloc-types-109-into.patch
Patch3009: Loongarch-Add-ifunc-support-for-strchr-aligned-lsx-l.patch
Patch3010: Loongarch-Add-ifunc-support-for-memcpy-aligned-unali.patch
Patch3011: LoongArch-Add-ifunc-support-for-strnlen-aligned-lsx-.patch
Patch3012: LoongArch-Add-ifunc-support-for-strcmp-aligned-lsx.patch
Patch3013: LoongArch-Add-ifunc-support-for-strncmp-aligned-lsx.patch
Patch3014: LoongArch-Remove-support-code-for-old-linker-in-star.patch
Patch3015: LoongArch-Micro-optimize-LD_PCREL.patch
Patch3016: LoongArch-Add-ifunc-support-for-rawmemchr-aligned-ls.patch
Patch3017: LoongArch-Add-ifunc-support-for-memchr-aligned-lsx-l.patch
Patch3018: LoongArch-Add-ifunc-support-for-memrchr-lsx-lasx.patch
Patch3019: LoongArch-Add-ifunc-support-for-memset-aligned-unali.patch
Patch3020: LoongArch-Add-ifunc-support-for-memcmp-aligned-lsx-l.patch
Patch3021: LoongArch-Change-loongarch-to-LoongArch-in-comments.patch
Patch3022: LoongArch-Add-lasx-lsx-support-for-_dl_runtime_profi.patch
Patch3023: LoongArch-Replace-deprecated-v0-with-a0-to-eliminate.patch
Patch3024: LoongArch-Add-ifunc-support-for-strcpy-stpcpy-aligne.patch
Patch3025: LoongArch-Add-ifunc-support-for-strrchr-aligned-lsx-.patch
Patch3026: LoongArch-Change-to-put-magic-number-to-.rodata-sect.patch
Patch3027: LoongArch-Add-glibc.cpu.hwcap-support.patch
Patch3028: Revert-LoongArch-Add-glibc.cpu.hwcap-support.patch
Patch3029: LoongArch-Unify-Register-Names.patch
Patch3030: LoongArch-Update-hwcap.h-to-sync-with-LoongArch-kern.patch
Patch3031: linux-Sync-Linux-6.6-elf.h.patch
Patch3032: Decrease-value-of-arch_minimum_kernel-with-LoongArch.patch
#--
Patch3033: 0001-LoongArch-Use-builtins-for-ffs-and-ffsll.patch
Patch3034: 0002-elf-Add-new-LoongArch-reloc-types-110-to-126-into-el.patch
Patch3035: 0003-LoongArch-Add-glibc.cpu.hwcap-support.patch
Patch3036: 0004-LoongArch-Add-support-for-TLS-Descriptors.patch
Patch3037: 0005-LoongArch-Fix-tst-gnu2-tls2-compiler-error.patch
Patch3038: 0006-LoongArch-Use-fcsr0-instead-of-r0-in-_FPU_-GET-SET-C.patch
Patch3039: 0007-LoongArch-Ensure-sp-16-byte-aligned-for-tlsdesc.patch
Patch3040: 0008-LoongArch-Fix-_dl_tlsdesc_dynamic-in-LSX-case.patch
Patch3041: 0009-LoongArch-Fix-tst-gnu2-tls2-test-case.patch
Patch3042: 0010-LoongArch-Add-cfi-instructions-for-_dl_tlsdesc_dynam.patch
Patch3043: 0011-LoongArch-Fix-macro-redefined-warning-in-tls-desc.S.patch
Patch3044: 0012-LoongArch-Undef-__NR_fstat-and-__NR_newfstatat.patch
Patch3045: 0013-From-Adhemerval-Zanella-adhemerval.zanella-linaro.or.patch
Patch3046: 0014-loongarch-Remove-duplicate-strnlen-in-libc.a-BZ-3178.patch
Patch3047: 0015-LoongArch-Change-tunable-for-2.38.patch
Patch3048: 0016-LoongArch-Simplify-the-autoconf-check-for-static-PIE.patch
Patch3049: 0017-nptl-Add-thread_pointer.h-for-LoongArch.patch
Patch3050: 0018-nptl-fix-__builtin_thread_pointer-detection-on-Loong.patch
Patch3051: LoongArch-Force-SHMLBA-the-same-as-kernel.patch

Patch5000: 5000-one-china-principle.patch

Provides: ldconfig, rtld(GNU_HASH)
Provides: /sbin/ldconfig
Provides: /usr/sbin/ldconfig

BuildRequires: glibc-devel procps-ng, util-linux, gawk, systemtap-sdt-devel
BuildRequires: audit-libs-devel >= 1.1.3, sed >= 3.95, libcap-devel, gettext, bison >= 2.7
BuildRequires: python3 python3-devel, gcc >= 7.2.1-6, make >= 4.0, binutils >= 2.30-17

%if %{without bootstrap}
BuildRequires: gd-devel libpng-devel zlib-devel
BuildRequires: libselinux-devel >= 1.33.4-3

%if %{with valgrind}
BuildRequires: valgrind
%endif

%if %{with testsuite}
BuildRequires: diffutils gcc-c++ libstdc++-static glibc-static libidn2 perl-interpreter
%endif
%endif

Requires: libgcc, glibc-common = %{version}-%{release}, basesystem
%if %{with all_langpack}
Requires: glibc-langpack-zh = %{version}-%{release}
Requires: glibc-langpack-en = %{version}-%{release}
%endif
Recommends: glibc-gconv-extra = %{version}-%{release}
Requires: (glibc-gconv-extra = %{version}-%{release} if system-rpm-config)

%description
The GNU C Library project provides the core libraries for the GNU
system and GNU/Linux systems, as well as many other systems that use
Linux as the kernel. These libraries provide critical APIs including
ISO C11, POSIX.1-2008, BSD, OS-specific APIs and more. These APIs
include such foundational facilities as open, read, write, malloc,
printf, getaddrinfo, dlopen, pthread_create, crypt, login, exit and more.

%package -n libnsl
Summary: Legacy support library for NIS
Requires: %{name} = %{version}-%{release}

%description -n libnsl
This package provides libnsl library for accessing NIS services.
If you want to use IPV6, try libnsl2.

%package devel
Summary: Object files for development using standard C libraries.
BuildRequires: kernel-headers >= 3.2
Requires: %{name} = %{version}-%{release}
Requires: libxcrypt-devel >= 4.0.0
Requires: kernel-headers >= 3.2
Provides: %{name}-headers = %{version}-%{release}
Provides: %{name}-static = %{version}-%{release}

%description devel
This package provides object and header files for developers who
use glibc libraries.

%package common
Summary: Common binaries and locale data for glibc
Requires: %{name} = %{version}-%{release}
Requires: tzdata >= 2003a

%description common
The glibc-common package includes common binaries for the GNU libc
libraries, as well as national language (locale) support.

%transfiletriggerin common -P 2000000 -p <lua> -- /lib /usr/lib /lib64 /usr/lib64
%glibc_post_funcs
call_ldconfig()
%end

%transfiletriggerpostun common -P 2000000 -p <lua> -- /lib /usr/lib /lib64 /usr/lib64
%glibc_post_funcs
call_ldconfig()
%end

%undefine __brp_ldconfig

%package locale-source
Summary: The sources for the locales
Requires: %{name} = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}

%description locale-source
This package provides sources of all locales in all-langpacks.

%if %{with all_langpack}
%{lua:
local lua_file = rpm.expand("%{SOURCE5}")
dofile(lua_file)
}
%endif

%package all-langpacks
Summary: All language packs for %{name}.
Requires: %{name} = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}
Provides: %{name}-langpack = %{version}-%{release}

%description all-langpacks
This package provides 'ALL' locales which can be used in GNU C library
internationally.

%package gconv-extra
Summary: All iconv converter modules for %{name}.
Requires: %{name} = %{version}-%{release}
Requires: %{name}-common = %{version}-%{release}

%description gconv-extra
This package providess all iconv converter modules built in GNU C library.

%if %{with nscd}
%package -n nscd
Summary:  Name caching service daemon.
BuildRequires: systemd-rpm-macros
Requires: %{name} = %{version}-%{release}
Requires: libselinux >= 1.17.10
Requires: audit-libs >= 1.1.3
Requires: systemd
Requires(pre): shadow-utils, coreutils
Requires(postun): shadow-utils

%description -n nscd
The nscd package is able to daemon caches name service lookups and improve
the performance with LDAP.
%endif

%package nss
Summary: Name Service Switch (NSS) module using hash-indexed files
Requires: %{name} = %{version}-%{release}
Provides: nss_db = %{version}-%{release}
Provides: nss_hesiod = %{version}-%{release}

%description nss
This package provides the ability of name lookup with the help of db.

%package nss-devel
Summary: Development files for directly linking NSS service modules
Requires: %{name} = %{version}-%{release}
Requires: nss_db = %{version}-%{release}
Requires: nss_hesiod = %{version}-%{release}

%description nss-devel
This package provides object files to developers that need to use
nss.

%package utils
Summary: Development utilities from GNU C library
Requires: %{name} = %{version}-%{release}

%description utils
This package provides some tools which can be used when debugging.

%if %{with benchtests}
%package benchtests
Summary: Benchmarking binaries and scripts for %{name}

%description benchtests
This package provides some build-in benchmark testcases which can be run
on system.
%endif

%if %{with source}
%package source
Summary: source included %{_vendor} patch
BuildRequires: tar, xz
BuildArch: noarch

%description source
This package provides source included %{_vendor} patch
for cross toolchains
%endif

%prep
%autosetup -n %{name}-%{version} -p1

chmod +x benchtests/scripts/*.py scripts/pylint

find . -type f -size 0 -o -name "*.orig" -delete

touch `find . -name configure`

touch locale/programs/*-kw.h

# rewriting the definition of __debug_install_post. 
# reserve the debuginfo in ld.so
%{lua:
local wrapper = rpm.expand("%{SOURCE4}")
local sysroot = rpm.expand("%{buildroot}")
local original = rpm.expand("%{macrobody:__debug_install_post}")
-- Strip leading newline.  It confuses the macro redefinition.
-- Avoid embedded newlines that confuse the macro definition.
original = original:match("^%s*(.-)%s*$"):gsub("\\\n", "")
rpm.define("__debug_install_post bash " .. wrapper
  .. " " .. sysroot .. " " .. original)
}

%build
%if %{with source}
tar acf %{name}-%{version}-%{release}.tar.xz *
%endif

reference=" \
        "-Wp,-D_GLIBCXX_ASSERTIONS" \
        "-fasynchronous-unwind-tables" \
        "-fstack-clash-protection" \
        "-funwind-tables" \
        "-m64" \
        "-march=armv8-a+lse" \
        "-march=armv8.1-a" \
        "-march=haswell" \
        "-march=x86-64" \
        "-march=x86-64-v2" \
        "-march=x86-64-v3" \
        "-march=x86-64-v4" \
        "-mcpu=power8" \
        "-mcpu=power9" \
        "-mbranch-protection=standard" \
        "-mfpmath=sse" \
        "-msse2" \
        "-mstackrealign" \
        "-mtune=generic" \
        "-mtune=power8" \
        "-mtune=power9" "

BuildFlags="-O2 -g"
for flag in $RPM_OPT_FLAGS $RPM_LD_FLAGS ; do
    if echo "$reference" | grep -q -F " $flag " ; then
        BuildFlags="$BuildFlags $flag"
    fi
done

rm -rf build-%{target}
mkdir build-%{target}
pushd build-%{target}
	
%if 0%{?_annotated_build} > 0
# this will enable annobin plugin of gcc but it will not work.
BuildFlagsNonshared = "-fplugin=annobin -fplugin-arg-annobin-disable -Wa,--generate-missing-build-notes=yes"
%endif

# mitigate performance issue for some intel platforms.
%ifarch x86_64
BuildFlags="$BuildFlags -Wa,-mbranches-within-32B-boundaries"
%endif

../configure CC=gcc CXX=g++ CFLAGS="$BuildFlags" \
	--prefix=%{_prefix} \
	--with-headers=%{_prefix}/include \
	--enable-kernel=3.2 \
	--with-nonshared-cflags="${BuildFlagsNonshared}" \
	--enable-bind-now \
	--build=%{target} \
	--enable-stack-protector=strong \
	--enable-multi-arch \
	--enable-systemtap \
%ifarch x86_64
	--enable-cet \
%endif
%if %{without werror}
	--disable-werror \
%endif
%if %{with bootstrap}
	--without-selinux \
%endif
	--disable-profile \
%ifarch aarch64
	--enable-memory-tagging \
%endif
	--disable-crypt \
%if %{without nscd}
	--disable-build-nscd \
	--disable-nscd 
%endif
    ||
	{ cat config.log; false; }

# in commit e47de5cb (Do not use --hash-style=both for building glibc shared objects),
# glibc upstream remove default --hash-style=both. We keep in spec to maintain compatibility.
# However, we have no commitment to maintain this operation permanently, we suggest all
# the user consider to switch to --hash-style=gnu
env LDFLAGS.so="-Wl,--hash-style=both" \
    LDFLAGS-rtld="-Wl,--hash-style=both" \
%make_build -r ASFLAGS="-g -Wa,--generate-missing-build-notes=yes"
popd

%install

%ifarch riscv64
for d in %{buildroot}%{_libdir} %{buildroot}/%{_lib}; do
	mkdir -p $d
	(cd $d && ln -sf . lp64d)
done
%endif

pushd build-%{target}
%make_build install_root=%{buildroot} install
%make_build install_root=%{buildroot} \
	install-locale-files -C ../localedata objdir=`pwd`
popd
hardlink %{buildroot}/usr/lib/locale

rm -f %{buildroot}/%{_libdir}/libNoVersion*
rm -f %{buildroot}/%{_lib}/libNoVersion*
rm -f %{buildroot}/%{_lib}/libnss1-*
rm -f %{buildroot}/%{_lib}/libnss-*.so.1
rm -f %{buildroot}/usr/sbin/sln
rm -f %{buildroot}/sbin/sln

mkdir -p %{buildroot}/var/cache/ldconfig
touch %{buildroot}/var/cache/ldconfig/aux-cache

%{buildroot}/sbin/ldconfig -N -r %{buildroot}

olddir=`pwd`
pushd %{buildroot}%{_prefix}/lib/locale
rm -f locale-archive
$olddir/build-%{target}/elf/ld.so \
        --library-path $olddir/build-%{target}/ \
        $olddir/build-%{target}/locale/localedef \
	--alias-file=$olddir/intl/locale.alias \
        --prefix %{buildroot} --add-to-archive \
        %locale_rx

ln locale-archive locale-archive.real
popd

pushd %{buildroot}/usr/lib/locale
for f in $(find %locale_rx -samefile C.utf8/LC_CTYPE); do
  rm $f && ln -s '../C.utf8/LC_CTYPE' $f
done
popd

%if %{with all_langpack}
pushd %{buildroot}%{_prefix}/lib/locale
for i in %locale_rx
do
    lang=${i%%_*}
    if [ ! -e langpack-${lang}.filelist ]; then
        echo "%dir %{_prefix}/lib/locale" >> langpack-${lang}.filelist
    fi
    echo "%dir  %{_prefix}/lib/locale/$i" >> langpack-${lang}.filelist
    echo "%{_prefix}/lib/locale/$i/*" >> langpack-${lang}.filelist
done
popd

pushd %{buildroot}%{_prefix}/share/locale
for i in */LC_MESSAGES/libc.mo
do
    locale=${i%%%%/*}
    lang=${locale%%%%_*}
    echo "%lang($lang) %{_prefix}/share/locale/${i}" \
         >> %{buildroot}%{_prefix}/lib/locale/langpack-${lang}.filelist
done
popd
mv  %{buildroot}%{_prefix}/lib/locale/*.filelist .
%else
for dir in %{buildroot}%{_prefix}/lib/locale/*; do
  if [ -d "$dir" ] && [ "$(basename "$dir")" != "C.utf8" ]; then
    rm -r "$dir"
  fi
done
%endif

%if %{with nscd}
install -m644 nscd/nscd.conf %{buildroot}/etc
mkdir -p %{buildroot}%{_tmpfilesdir}
install -m644 %{SOURCE6} %{buildroot}%{_tmpfilesdir}
mkdir -p %{buildroot}/lib/systemd/system
install -m 644 nscd/nscd.service %{buildroot}/lib/systemd/system

mkdir -p %{buildroot}/var/{db,run}/nscd
touch %{buildroot}/var/{db,run}/nscd/{passwd,group,hosts,services}
touch %{buildroot}/var/run/nscd/nscd.pid
%endif

touch %{buildroot}/etc/ld.so.cache
echo 'include ld.so.conf.d/*.conf' > %{buildroot}/etc/ld.so.conf
chmod 644 %{buildroot}/etc/ld.so.conf
mkdir -p %{buildroot}/etc/ld.so.conf.d
touch %{buildroot}/etc/gai.conf
touch %{buildroot}%{_libdir}/gconv/gconv-modules.cache
chmod 644 %{buildroot}%{_libdir}/gconv/gconv-modules.cache

rm -rf %{buildroot}%{_prefix}/share/zoneinfo

touch -r %{SOURCE0} %{buildroot}/etc/ld.so.conf
touch -r inet/etc.rpc %{buildroot}/etc/rpc

%if %{with benchtests}
pushd build-%{target}
make BENCH_DURATION=1 bench-build
popd

mkdir -p %{buildroot}%{_prefix}/libexec/glibc-benchtests
cp $(find build-%{target}/benchtests -type f -executable) %{buildroot}%{_prefix}/libexec/glibc-benchtests/
cp {%{SOURCE1},%{SOURCE2}} %{buildroot}%{_prefix}/libexec/glibc-benchtests/
cp benchtests/scripts/{benchout.schema.json,benchout.schema.json,compare_bench.py,import_bench.py,validate_benchout.py} \
 %{buildroot}%{_prefix}/libexec/glibc-benchtests/
%endif

%if %{without fastbuild}
pushd locale
ln -s programs/*.gperf .
popd
pushd iconv
ln -s ../locale/programs/charmap-kw.gperf .
popd
%endif

mkdir -p %{buildroot}%{_libdir}
mv -f %{buildroot}/%{_lib}/lib{pcprofile,memusage}.so \
	%{buildroot}%{_libdir}

strip -g %{buildroot}%{_libdir}/*.o

for i in %{buildroot}%{_prefix}/bin/{xtrace,memusage}; do
%if %{with bootstrap}
  test -w $i || continue
%endif
  sed -e 's#=/%{_lib}\/libpcprofile.so#=%{_libdir}/libpcprofile.so#' \
      -e 's#=/%{_lib}/libmemusage.so#=%{_libdir}/libmemusage.so#' \
      -e 's#='\''/\\\$LIB/libpcprofile.so#='\''%{_prefix}/\\$LIB/libpcprofile.so#' \
      -e 's#='\''/\\\$LIB/libmemusage.so#='\''%{_prefix}/\\$LIB/libmemusage.so#' \
      -i $i
done

{
  find %{buildroot} \( -type f -o -type l \) \
       \( \
	 -name etc -printf "%%%%config " -o \
	 -name gconv-modules.cache \
	 -printf "%%%%verify(not md5 size mtime) " -o \
	 -name gconv-modules* \
	 -printf "%%%%verify(not md5 size mtime) %%%%config(noreplace) " \
	 , \
	 ! -path "*/lib/debug/*" -printf "/%%P\n" \)
  find %{buildroot} -type d \
       \( -path '*%{_prefix}/share/locale' -prune -o \
       \( -path '*%{_prefix}/share/*' -o \
	  -path "*%{_prefix}/include/*" \
       \) -printf "%%%%dir /%%P\n" \)
} | {
  sed -e '\,.*/share/locale/\([^/_]\+\).*/LC_MESSAGES/.*\.mo,d' \
      -e '\,.*/share/i18n/locales/.*,d' \
      -e '\,.*/share/i18n/charmaps/.*,d' \
      -e '\,.*/etc/\(localtime\|nsswitch.conf\|ld\.so\.conf\|ld\.so\.cache\|default\|rpc\|gai\.conf\),d' \
      -e '\,.*/%{_libdir}/lib\(pcprofile\|memusage\)\.so,d' \
      -e '\,.*/bin/\(memusage\|mtrace\|xtrace\|pcprofiledump\),d'
} | sort > master.filelist

chmod 0444 master.filelist

cat master.filelist \
	| grep -v \
	-e '%{_libdir}/lib.*_p.a' \
	-e '%{_prefix}/include' \
	-e '%{_libdir}/lib.*\.a' \
        -e '%{_libdir}/.*\.o' \
	-e '%{_libdir}/lib.*\.so' \
	-e '%{_libdir}/gconv/.*\.so$' \
	-e '%{_libdir}/gconv/gconv-modules.d/gconv-modules-extra\.conf$' \
	-e '%{_prefix}/bin' \
	-e '%{_prefix}/lib/locale' \
	-e '%{_prefix}/sbin/[^i]' \
	-e '%{_prefix}/share' \
	-e '/var/db/Makefile' \
	-e '/libnss_.*\.so[0-9.]*$' \
	-e 'glibc-benchtests' \
%if %{with nscd}
    -e 'nscd' \
%endif
	-e 'aux-cache' \
	> glibc.filelist

grep -e "gconv-modules-extra.conf" master.filelist > gconv.filelist

GconvBaseModules="ISO8859-15 ISO8859-1 CP1252"
GconvBaseModules="$GconvBaseModules UNICODE UTF-16 UTF-32 UTF-7"
GconvAllModules=$(cat master.filelist |
                 sed -n 's|%{_libdir}/gconv/\(.*\)\.so|\1|p')

# Put the base modules into glibc and the rest into glibc-gconv-extra
for conv in $GconvAllModules; do
    if echo $GconvBaseModules | grep -q $conv; then
        grep -E -e "%{_libdir}/gconv/$conv.so$" \
            master.filelist >> glibc.filelist
    else
        grep -E -e "%{_libdir}/gconv/$conv.so$" \
            master.filelist >> gconv.filelist
    fi
done

for module in compat files dns; do
    cat master.filelist \
	| grep -E \
	-e "/libnss_$module(\.so\.[0-9.]+|-[0-9.]+\.so)$" \
	>> glibc.filelist
done
grep -e "libmemusage.so" -e "libpcprofile.so" master.filelist >> glibc.filelist

grep '%{_libdir}/lib.*\.a' master.filelist  > devel.filelist
grep '%{_libdir}/.*\.o' < master.filelist >> devel.filelist
grep '%{_libdir}/lib.*\.so' < master.filelist >> devel.filelist
sed -i -e '\,libmemusage.so,d' \
	-e '\,libpcprofile.so,d' \
	-e '\,/libnss_[a-z]*\.so$,d' \
	devel.filelist
grep '%{_prefix}/include' master.filelist >> devel.filelist


grep '%{_prefix}/bin' master.filelist >> common.filelist
grep '%{_prefix}/sbin' master.filelist \
	| grep -v '%{_prefix}/sbin/iconvconfig' \
%if %{with nscd}
    | grep -v 'nscd' \
%endif
    >> common.filelist
grep '%{_prefix}/share' master.filelist \
	| grep -v \
	-e '%{_prefix}/share/info/libc.info.*' \
	-e '%%dir %{prefix}/share/info' \
	-e '%%dir %{prefix}/share' \
	-e '%{_docdir}' \
	>> common.filelist

cat > utils.filelist <<EOF
%if %{without bootstrap}
%{_prefix}/bin/memusage
%{_prefix}/bin/memusagestat
%endif
%{_prefix}/bin/mtrace
%{_prefix}/bin/pcprofiledump
%{_prefix}/bin/xtrace
EOF

grep -E "/libnss_(db|hesiod)\\.so\\.[0-9.]+\$" master.filelist > nss.filelist
grep -E "%{_prefix}/bin/makedb$" master.filelist >> nss.filelist

grep '/libnss_[a-z]*\.so$' master.filelist > nss-devel.filelist

%if %{with benchtests}
find build-%{target}/benchtests -type f -executable | grep -w -v '.build' | while read b; do
	echo "%{_prefix}/libexec/glibc-benchtests/$(basename $b)"
done >> benchtests.filelist

echo "%{_prefix}/libexec/glibc-benchtests/$(basename %{SOURCE1})" >> benchtests.filelist
echo "%{_prefix}/libexec/glibc-benchtests/$(basename %{SOURCE2})" >> benchtests.filelist
echo "%{_prefix}/libexec/glibc-benchtests/benchout.schema.json" >> benchtests.filelist
echo "%{_prefix}/libexec/glibc-benchtests/compare_bench.py*" >> benchtests.filelist
echo "%{_prefix}/libexec/glibc-benchtests/import_bench.py*" >> benchtests.filelist
echo "%{_prefix}/libexec/glibc-benchtests/validate_benchout.py*" >> benchtests.filelist
%endif

echo '%{_prefix}/sbin/nscd' > nscd.filelist

mkdir -p %{buildroot}%{_usrsrc}/%{name}
%if %{with source}
cp -f %{name}-%{version}-%{release}.tar.xz  %{buildroot}%{_usrsrc}/%{name}/
%endif

%check
%if %{with testsuite}

pushd build-%{target}
export TIMEOUTFACTOR=16
parent=$$
%make_build check |& tee build_check.log >&2
if ! grep -q '^Summary of test results:$' build_check.log || ! test -s tests.sum ; then
  echo "FAILED: test suite build failed!" >& 2
  exit 1
fi
set +x
grep -v ^PASS: tests.sum > build.tests.sum.not-passing || true
if test -s build.tests.sum.not-passing ; then
  while read failed_code failed_test ; do
    for suffix in out test-result ; do
      if test -e "$failed_test.$suffix"; then
        echo >&2
        echo "=====$failed_code $failed_test.$suffix=====" >&2
        cat -- "$failed_test.$suffix" >&2
        echo >&2
      fi
    done
  done <build.tests.sum.not-passing
fi
set -x
popd

PLTCMD='/^Relocation section .*\(\.rela\?\.plt\|\.rela\.IA_64\.pltoff\)/,/^$/p'
echo ====================PLT RELOCS LD.SO================
readelf -Wr %{buildroot}/%{_lib}/ld-*.so | sed -n -e "$PLTCMD"
echo ====================PLT RELOCS LIBC.SO==============
readelf -Wr %{buildroot}/%{_lib}/libc-*.so | sed -n -e "$PLTCMD"
echo ====================PLT RELOCS END==================
 
# run the dynamic loader.
ldso_path="$(find %{buildroot}/ -regextype posix-extended \
  -regex '.*/ld(-.*|64|)\.so\.[0-9]+$' -type f | LC_ALL=C sort | head -n1)"
run_ldso="$ldso_path --library-path %{buildroot}/%{_lib}"

LD_SHOW_AUXV=1 $run_ldso /bin/true

%if 0%{?_enable_debug_packages}
# Finally, check if valgrind runs with the new glibc.
# We want to fail building if valgrind can not run with this glibc so
# that we can fix it before updating glibc.

%if %{with valgrind}
$run_ldso /usr/bin/valgrind --error-exitcode=1 \
	$run_ldso /usr/bin/true
# true --help performs some memory allocations.
$run_ldso /usr/bin/valgrind --error-exitcode=1 \
	$run_ldso /usr/bin/true --help >/dev/null

# with valgrind
%endif

# _enable_debug_packages
%endif

# with testsuite
%endif

%if %{with nscd}
%pre -n nscd
getent group nscd >/dev/null || /usr/sbin/groupadd -g 28 -r nscd
getent passwd nscd >/dev/null ||
  /usr/sbin/useradd -M -o -r -d / -s /sbin/nologin \
		    -c "NSCD Daemon" -u 28 -g nscd nscd

%post -n nscd
%systemd_post nscd.service

%preun -n nscd
%systemd_preun nscd.service

%postun -n nscd
if test $1 = 0; then
  /usr/sbin/userdel nscd > /dev/null 2>&1 || :
fi
%systemd_postun_with_restart nscd.service
%endif

%post -p <lua>
%glibc_post_funcs
-- (1) Remove multilib libraries from previous installs.
install_libs = { "anl", "BrokenLocale", "c", "dl", "m", "mvec",
                 "nss_compat", "nss_db", "nss_dns", "nss_files",
                 "nss_hesiod", "pthread", "resolv", "rt", "SegFault",
                 "thread_db", "util" }

remove_regexps = {}
for i = 1, #install_libs do
  remove_regexps[#remove_regexps + 1] = ("lib" .. install_libs[i]
                                         .. "%%-[2-9]%%.[0-9]+%%.so$")
  remove_regexps[#remove_regexps + 1] = ("lib" .. install_libs[i]
                                         .. "%%.so%%.[0-9]+$")
end

remove_regexps[#install_libs + 1] = "libthread_db%%-1%%.0%%.so"
remove_regexps[#install_libs + 2] = "libSegFault%%.so"

local remove_dirs = { "%{_libdir}/i686",
                      "%{_libdir}/i686/nosegneg",
                      "%{_libdir}/power6",
                      "%{_libdir}/power7",
                      "%{_libdir}/power8",
                      "%{_libdir}/power9",
                    }

repeat
  local iter = posix.files("%{_libdir}/glibc-hwcaps")
  if iter ~= nil then
    for entry in iter do
      if entry ~= "." and entry ~= ".." then
        local path = "%{_libdir}/glibc-hwcaps/" .. entry
        if posix.access(path .. "/.", "x") then
          remove_dirs[#remove_dirs + 1] = path
        end
      end
    end
  end
until true

for _, rdir in ipairs (remove_dirs) do
  if posix.access (rdir) then
    local remove_files = posix.files (rdir)
    for rfile in remove_files do
      for _, rregexp in ipairs (remove_regexps) do
        local dso = string.match (rfile, rregexp)
        if (dso ~= nil) then
          os.remove (rdir .. '/' .. rfile)
        end
      end
    end
  end
end

-- (2) Update /etc/ld.so.conf
local ldsoconf = "/etc/ld.so.conf"
local ldsoconf_tmp = "/etc/glibc_post_upgrade.ld.so.conf"

if posix.access (ldsoconf) then

  local have_include = false
  for line in io.lines (ldsoconf) do
    if string.match (line, "^include ld.so.conf.d/%%*%%.conf$") ~= nil then
      have_include = true
    end
  end

  if not have_include then
    local tmp_fd = io.open (ldsoconf_tmp, "w")
    if tmp_fd ~= nil then
      tmp_fd:write ("include ld.so.conf.d/*.conf\n")
      for line in io.lines (ldsoconf) do
        tmp_fd:write (line .. "\n")
      end
      tmp_fd:close ()
      local res = os.rename (ldsoconf_tmp, ldsoconf)
      if res == nil then
        io.stdout:write ("Error: Unable to update configuration file (rename).\n")
      end
    else
      io.stdout:write ("Error: Unable to update configuration file (open).\n")
    end
  end
end

-- (3) Rebuild ld.so.cache early.
call_ldconfig()

-- (4) Update gconv modules cache.
update_gconv_modules_cache()

-- (5) On upgrades, restart systemd if installed.  "systemctl -q" does
if tonumber(arg[2]) >= 2
   and posix.access("%{_prefix}/bin/systemctl", "x")
then
  local pid = posix.fork()
  if pid == 0 then
    posix.redirect2null(2)
    posix.exec("%{_prefix}/bin/systemctl", "daemon-reexec")
  elseif pid > 0 then
    posix.wait(pid)
  end
end

%posttrans all-langpacks -e -p <lua>
local archive_path = "%{_prefix}/lib/locale/locale-archive"
local real_path = "%{_prefix}/lib/locale/locale-archive.real"
local stat_archive = posix.stat(archive_path)
local stat_real = posix.stat(real_path)
if stat_archive ~= nil and stat_real ~= nil
    and (stat_archive.ino ~= stat_real.ino
         or stat_archive.dev ~= stat_real.dev) then
  posix.unlink(archive_path)
  stat_archive = nil
end
if stat_archive == nil then
  posix.link(real_path, archive_path)
end
local save_path = archive_path .. ".rpmsave"
if posix.access(save_path) then
  posix.unlink(save_path)
end

%post gconv-extra -p <lua>
%glibc_post_funcs
update_gconv_modules_cache ()

%postun gconv-extra -p <lua>
%glibc_post_funcs
update_gconv_modules_cache ()

%files -f glibc.filelist
%license COPYING COPYING.LIB LICENSES
%dir %{_prefix}/%{_lib}/audit
%verify(not md5 size mtime) %config(noreplace) /etc/ld.so.conf
%verify(not md5 size mtime) %config(noreplace) /etc/rpc
%dir /etc/ld.so.conf.d
%dir %{_prefix}/libexec/getconf
%dir %{_libdir}/gconv
%dir %{_libdir}/gconv/gconv-modules.d
%dir %attr(0700,root,root) /var/cache/ldconfig
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/cache/ldconfig/aux-cache
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /etc/ld.so.cache
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /etc/gai.conf

%files -f common.filelist common
%dir %{_prefix}/lib/locale
%dir %{_prefix}/lib/locale/C.utf8
%{_prefix}/lib/locale/C.utf8/*

%files all-langpacks
%{_prefix}/lib/locale/locale-archive
%{_prefix}/lib/locale/locale-archive.real
%{_prefix}/share/locale/*/LC_MESSAGES/libc.mo

%files locale-source
%dir %{_prefix}/share/i18n/locales
%{_prefix}/share/i18n/locales/*
%dir %{_prefix}/share/i18n/charmaps
%{_prefix}/share/i18n/charmaps/*

%files -f devel.filelist devel

%files -f utils.filelist utils

%files -f gconv.filelist gconv-extra

%if %{with nscd}
%files -f nscd.filelist -n nscd
%config(noreplace) /etc/nscd.conf
%dir %attr(0755,root,root) /var/run/nscd
%dir %attr(0755,root,root) /var/db/nscd
/lib/systemd/system/nscd.service
%{_tmpfilesdir}/nscd.conf
%attr(0644,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/nscd.pid
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/passwd
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/group
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/hosts
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/run/nscd/services
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/db/nscd/passwd
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/db/nscd/group
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/db/nscd/hosts
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/db/nscd/services
%ghost %config(missingok,noreplace) /etc/sysconfig/nscd
%endif

%files -f nss.filelist nss
/var/db/Makefile

%files -f nss-devel.filelist nss-devel

%if %{with benchtests}
%files -f benchtests.filelist benchtests
%endif

%if %{with source}
%files source
%{_usrsrc}/%{name}/%{name}-%{version}-%{release}.tar.xz
%endif

%changelog
* Thu Jan 02 2025 Tracker Robot <trackbot@opencloudos.tech> - 2.38-28
- [Type] bugfix
- [DESC] Apply patches from rpm-tracker
- [Performance] 950891b5e7a5307272da3e632832ac9da4c9eeec.patch: Optimize bsearch() implementation for performance
- [Bug Fix] 6fd215d6ae9a4a6e75f7ea18d89db6a10f158eaf.patch: posix: fix system when a child cannot be created [BZ #32450]

* Thu Jan 02 2025 Peng Fan <fanpeng@loongson.cn> - 2.38-27
- LoongArch: Force SHMLBA the same as kernel

* Wed Nov 27 2024 Peng Fan <fanpeng@loongson.cn> - 2.38-26
- LoongArch: Fix errors about PIE.

* Thu Nov 7 2024 Peng Fan <fanpeng@loongson.cn> - 2.38-25
- LoongArch: Sync from glibc upstream

* Tue Nov 5 2024 Shuo Wang <abushwang@tencent.com> - 2.38-24
- requires zh and en defaultly
- fix update glibc fail from old version to 2.38-16 when 'weakdependency=False'

* Tue Oct 8 2024 Shuo Wang <abushwang@tencent.com> - 2.38-23
- backport patch to fix str{n}cmp-evex performance degradation
- x86/string: Fixup alignment of main loop in str{n}cmp-evex [BZ #32212]

* Sun Sep 29 2024 Jian Zhao <jackjzhao@tencent.com> - 2.38-22
- [Type] other
- [DESC] Add a macro check for valgrind_arches to disable Valgrind tests if needed

* Thu Sep 26 2024 Xionghu Luo <xionghuluo@tencent.com> - 2.38-21
-  Add flag to mitigate Intel JCC performance issues.

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38-20
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38-19
- Rebuilt for loongarch release

* Wed Jul 31 2024 Shuo Wang <abushwang@tencent.com> - 2.38-18
- [Type] other
- [DESC] fix 'Failed to set locale'

* Mon Jul 29 2024 Shuo Wang <abushwang@tencent.com> - 2.38-17
- [Type] other
- [DESC] add fast build path

* Thu Jul 25 2024 Shuo Wang <abushwang@tencent.com> - 2.38-16
- [Type] other
- [DESC] re-write langpack-xx and gconv-extra sub packages

* Tue Jul 9 2024 Shuo Wang <abushwang@tencent.com> - 2.38-15
- [Type] other
- [DESC] mv tar cross toolchains to build step

* Thu Jun 27 2024 <doupengda@loongson.cn> - 2.38-14
- [Type] other
- [DESC] loongarch64 architecture disable check

* Mon Jun 17 2024 Rebuild Robot <rebot@opencloudos.org> - 2.38-13
- [Type] bugfix
- [DESC] Rebuilt for gd

* Fri May 31 2024 Shuo Wang <abushwang@tencent.com> - 2.38-12
- provides a new subpackage included patched source for cross toolchains

* Mon Apr 29 2024 Shuo Wang <abushwang@tencent.com> - 2.38-11
- backport patch to fix CVE-2024-33599 - CVE-2024-33601
- These patch fix buffers and null pointer crashes problem in netgroup

* Thu Apr 18 2024 Shuo Wang <abushwang@tencent.com> - 2.38-10
- backport patch to fix gcc --static too large

* Thu Apr 18 2024 Shuo Wang <abushwang@tencent.com> - 2.38-9
- add some macro for condition build

* Thu Apr 18 2024 Shuo Wang <abushwang@tencent.com> - 2.38-8
- backport patch to fix CVE-2024-2961
- fix out-of-bound writes when writing escape sequence

* Fri Mar 22 2024 Shuo Wang <abushwang@tencent.com> - 2.38-7
- fix lua error message in mock build

* Wed Mar 13 2024 Peng Fan <fanpeng@loongson.cn> - 2.38-6
- LoongArch: sync patch from glibc upstream
- Reduced kernel version requirements

* Thu Feb 1 2024 Shuo Wang <abushwang@tencent.com> - 2.38-5
- backport patch to fix CVE-2023-6246, CVE-2023-6779, CVE-2023-6780
- Fix heap buffer overflow for syslog

* Wed Oct 18 2023 Shuo Wang <abushwang@tencent.com> - 2.38-4
- backport patch to fix CVE-2023-4806 CVE-2023-5156 CVE-2023-4527

* Mon Oct 9 2023 Shuo Wang <abushwang@tencent.com> - 2.38-3
- backport patch to fix CVE-2023-4911

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38-2
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Aug 28 2023 Shuo Wang <abushwang@tencent.com> - 2.38-1
- update to 2.38

* Thu Aug 10 2023 Shuo Wang <abushwang@tencent.com> - 2.37-3
- fix CVE-2023-25139

* Fri Aug 04 2023 Xin Cheng <denisecheng@tencent.com> - 2.37-2
- add GB18030-2022 charmap

* Mon Jul 24 2023 Shuo Wang <abushwang@tencent.com> - 2.37-1
- update to 2.37

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.35-7
- Rebuilt for OpenCloudOS Stream 23.05

* Thu Apr 20 2023 cunshunxia <cunshunxia@tencent.com> - 2.35-6
- restore nscd.

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.35-5
- Rebuilt for OpenCloudOS Stream 23

* Thu Mar 9 2023 Shuo Wang <abushwang@tencent.com> - 2.35-4
- backport patch to fix shm_open errno no meet POSIX

* Mon Oct 10 2022 Yi Lin <nilusyi@tencent.com> - 2.35-3
- reserve debug symbol in ld.so

* Sat Oct 08 2022 Zhao Zhen <jeremiazhao@tencent.com> - 2.35-2
- disabled annobin to avoid compiling error

* Mon May 30 2022 cunshunxia <cunshunxia@tencent.com> - 2.35-1
- initial build
